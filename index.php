<?php
session_start();
require 'PHPMailerAutoload.php';

$conf["title"]="Cliente";
$conf["password"]="password";

if($_SESSION["login"]==$conf["title"]){

	//configurazioni
	$conf["dbhost"]="localhost";
	$conf["dbuser"]="dbuser";
	$conf["dbpass"]="dbpass";
	$conf["db"]="db";
	$conf["tabella"]="tabella";
	$conf["dbprivacy"]="dbtracker";

	$conf["host"]="mailsmtp";
	$conf["username"]="email";
	$conf["password"]="password";
	$conf["SMTPSecure"]="ssl";
	$conf["port"]=465;
	$conf["setfrom1"]="email";
	$conf["setfrom2"]="Name of email";

	$conf["modelli"]=array(
		array("nome"=>"Nome", "file"=>"file")
	);

//inizializza connessione al database
$mysqli=new mysqli($conf["dbhost"],$conf["dbuser"],$conf["dbpass"],$conf["db"]);
$mysqliPrivacy=new mysqli($conf["dbhost"],$conf["dbuser"],$conf["dbpass"],$conf["dbprivacy"]);

//TODO fare in modo che se la connessione al db fallisce lo script non si blocchi (try catch?)
if ($mysqli->connect_errno) {
	echo"<br><div class='alert alert-danger'>Connessione al database non riuscita</div><br>";
	exit(0);
}

//funzione che prende l'elenco dei destinatari, aggiunge le mail da inviare alla coda e poi tenta l'invio
function sendMailDB($conf,$mysqli){
	$destinatariText = isset($_POST['destinatari'])?$_POST['destinatari']:""; // recupera l'area di testo contenente i destinatari, poi trasforma in array

	if (strlen($destinatariText)==0) {
	  echo 'Nessun destinatario inserito.';
	  exit;
	}
	$destinatari = explode("\n", str_replace("\r", "", $destinatariText));

	foreach($destinatari as $destinatario){
		if($destinatario!=""){//evita che una riga vuota venga inserita
			$mysqli->query("INSERT INTO ".$conf["tabella"]." (destinatario, inviato, soggetto, testo, modello) VALUES ('".$destinatario."',FALSE,'".subject()."','".testoPersonalizzato()."','".$_POST["modello"]."')");
		}
	}
	coda($mysqli,$conf,$_POST["modello"]);
}

// funzione che invia una singola mail già presente nel database e aggiorna il suo stato sull'invio
function sendSingleMail($conf, $idmail, $mysqli){

	$result=$mysqli->query("SELECT * FROM ".$conf["tabella"]." WHERE idmail=".$idmail);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		$destinatario=$row["destinatario"];
		$soggetto=$row["modello"];
		$mail = new PHPMailer;
		$mail->setLanguage('it', 'language/');
		$mail->isSMTP(); // Set mailer to use SMTP
		$mail->Host = $conf["host"]; // Specify main and backup SMTP servers
		$mail->SMTPAuth = true; // Enable SMTP authentication
		$mail->Username = $conf["username"]; // SMTP username
		$mail->Password = $conf["password"]; // SMTP password
		$mail->SMTPSecure = $conf["SMTPSecure"]; // Enable SSL encryption, `tls` also accepted
		$mail->Port = $conf["port"]; // TCP port to connect to
		$mail->setFrom($conf["setfrom1"], $conf["setfrom2"]); //mittente mail, secondo parametro � il nome del mittente

		if($row["soggetto"]!="Default") $mail->Subject = $row["soggetto"];
		$alt_text=$row["testo"];
		$mail->isHTML(true); // Set email format to HTML
		include "modelli/" . $_POST["modello"] . ".php";

		// Invia la mail e aggiorna la coda
		$mail->addAddress($destinatario);
		if (!$mail->send()){
			echo 'Il messaggio non pu&ograve; essere inviato a ' . $destinatario . " . ";
			$mysqli->query("UPDATE ".$conf["tabella"]." SET errori='" . $mail->ErrorInfo . "' WHERE idmail=".$idmail);
			echo 'Errore: ' . $mail->ErrorInfo . "<br />";
		}else{
			$mysqli->query("UPDATE ".$conf["tabella"]." SET inviato=TRUE, datainvio=now() WHERE idmail=".$idmail);
			echo "Il messaggio e' stato inviato correttamente a " . $destinatario . '<br /><br />';
		}
		unset($mail);
	}
}

//funzione che rimanda alla pagina html dell'anteprima
function anteprima(){
	header('Location: '."modelli/".$_POST["modello"].".html");
}

//funzione che ritorna il soggetto alternativo usato o una stringa default se non è presente
function subject(){
	if (isset($_POST['subject']) && $_POST['subject']!=""){
		return $_POST['subject'];
	}else{
		return "Default";
	}
}

//funzione che ritorna il testo personalizzato o una stringa default se non è presente
function testoPersonalizzato(){
	if(isset($_POST["textedit"])&&$_POST["textedit"]!=""){
	    return $_POST["textedit"];
	}else{
	    return "Default";
	}
}

//funzione che ritorna un div in base ad un boolean
function inviato($inviato){
	if(!$inviato){
		return "<div class='alert alert-danger'>Non Inviato</div>";
	}
	else{
		return "<div class='alert alert-success'>Inviato</div>";
	}
}

//funzione che manda in output lo stato delle mail nella coda per il dato modello
function registro($mysqli,$conf,$modello){

	$count=$mysqli->query("SELECT modello, DATE(datainvio) AS DData, COUNT(*) AS NumInviato FROM ".$conf["tabella"]." WHERE datainvio<>'' GROUP BY DData, modello ORDER BY DData DESC");
	if ($count->num_rows > 0) {
		echo "<table class='table' style='padding: 0 3%;'><tr><th>Conteggio invii</th><th>Data invio</th><th>Soggetto</th></tr>";
		while($row = $count->fetch_assoc()) {
			echo "<tr id='countmail".$row["NumInviato"]."'><td>".$row["NumInviato"]."</td><td>".$row["DData"]."</td><td>".$row["modello"]."</td></tr>";
		}
		echo "</table>";
		echo "<br><br>";
	}
	else{
		echo "<br><br><div style='padding: 0 3%;'>Nessuna email è ancora stata confermata</div>";
	}
	$result=$mysqli->query("SELECT * FROM ".$conf["tabella"]." WHERE modello='".$modello."' ORDER BY idmail");
	if ($result->num_rows > 0) {
		echo "<br><br><table class='table' style='padding: 0 3%;'><tr><th>Id Mail</th><th>Destinatario</th><th>Inviato</th><th>Data Invio/Errori nell'invio</th><th>Soggetto</th><th></th></tr>";

		while($row = $result->fetch_assoc()) {
			if($row["inviato"]){
				echo "<tr id='mail".$row["idmail"]."'><td>".$row["idmail"]."</td><td>".$row["destinatario"]."</td><td>".inviato($row["inviato"])."</td><td>".$row["datainvio"]."</td><td>".$row["modello"]."</td></tr>";
			}else{
				echo "<tr id='mail".$row["idmail"]."'><td>".$row["idmail"]."</td><td>".$row["destinatario"]."</td><td>".inviato($row["inviato"])."</td><td>".$row["errori"]."</td><td>".$row["modello"]."</td><td><button type='button' class='btn btn-danger deleteButton' id='deleteButton".$row["idmail"]."'value='".$row["idmail"]."'>X</button></td></tr> ";
			}
		}
		echo "</table>";
	} else {
		echo "<br><br><div style='padding: 0 3%;'>Nessuna mail è ancora stata inviata</div>";
	}
}

//funzione che manda in output lo stato delle conferme per la privacy
function registroprivacy($conf,$mysqli){

	$count=$mysqli->query("SELECT DATE(Data) AS DData,Cliente, COUNT(*) AS NumClick, Soggetto FROM privacysubscriptions WHERE Cliente = 'alcafood' GROUP BY DData, Soggetto ORDER BY DData DESC");
	if ($count->num_rows > 0) {
		echo "<table class='table' style='padding: 0 3%;'><tr><th>Conteggio click per la privacy</th><th>Data click</th><th>Soggetto</th></tr>";
		while($row = $count->fetch_assoc()) {
			echo "<tr id='countmail".$row["NumClick"]."'><td>".$row["NumClick"]."</td><td>".$row["DData"]."</td><td>".$row['Soggetto']."</td></tr>";
		}
		echo "</table>";
		echo "<br><br>";
	}
	else{
		echo "<br><br><div style='padding: 0 3%;'>Nessuna email è ancora stata confermata</div>";
	}
	$result=$mysqli->query("SELECT * FROM privacysubscriptions WHERE Cliente = 'alcafood' ORDER BY Data DESC");
	if ($result->num_rows > 0) {
		echo "<br><br><table class='table' style='padding: 0 3%;'><tr><th>Email</th><th>Data conferma</th><th>Soggetto</th></tr>";
		while($row = $result->fetch_assoc()) {
			echo "<tr id='mail".$row["Email"]."'><td>".$row["Email"]."</td><td>".$row["Data"]."</td><td>".$row['Soggetto']."</td></tr>";
		}
		echo "</table>";
	} else {
		echo "<br><br><div style='padding: 0 3%;'>Nessuna email è ancora stata confermata</div>";
	}
}

//funzione che manda in output lo stato di apertura delle email
function registroapertura($conf,$mysqli){

	$count=$mysqli->query("SELECT DATE(Data) AS DData,Cliente, COUNT(*) AS NumAperto, Soggetto FROM trackopen WHERE Cliente = 'alcafood' GROUP BY DData, Soggetto ORDER BY DData DESC");
	if ($count->num_rows > 0) {
		echo "<table class='table' style='padding: 0 3%;'><tr><th>Conteggio aperture</th><th>Data apertura</th><th>Soggetto</th></tr>";
		while($row = $count->fetch_assoc()) {
			echo "<tr id='countmail".$row["NumAperto"]."'><td>".$row["NumAperto"]."</td><td>".$row["DData"]."</td><td>".$row['Soggetto']."</td></tr>";
		}
		echo "</table>";
		echo "<br><br>";
	}
	else{
		echo "<br><br><div style='padding: 0 3%;'>Nessuna email è ancora stata aperta</div>";
	}
	$result=$mysqli->query("SELECT * FROM trackopen WHERE Cliente = 'alcafood' ORDER BY Data DESC");
	if ($result->num_rows > 0) {
		echo "<br><br><table class='table' style='padding: 0 3%;'><tr><th>Email</th><th>Data apertura</th><th>Soggetto</th></tr>";
		while($row = $result->fetch_assoc()) {
			echo "<tr id='mail".$row["Email"]."'><td>".$row["Email"]."</td><td>".$row["Data"]."</td><td>".$row['Soggetto']."</td></tr>";
		}
		echo "</table>";
	} else {
		echo "<br><br><div style='padding: 0 3%;'>Nessuna email è ancora stata aperta</div>";
	}
}

//funzione che manda in output lo stato di azione delle email
function registroazione($conf,$mysqli){

	$count=$mysqli->query("SELECT DATE(Data) AS DData,Cliente, COUNT(*) AS NumAzione, Soggetto FROM tracklink WHERE Cliente = 'alcafood' GROUP BY DData, Soggetto ORDER BY DData DESC");
	if ($count->num_rows > 0) {
		echo "<table class='table' style='padding: 0 3%;'><tr><th>Conteggio azioni</th><th>Data azione (click su link dem)</th><th>Soggetto</th></tr>";
		while($row = $count->fetch_assoc()) {
			echo "<tr id='countmail".$row["NumAzione"]."'><td>".$row["NumAzione"]."</td><td>".$row["DData"]."</td><td>".$row['Soggetto']."</td></tr>";
		}
		echo "</table>";
		echo "<br><br>";
	}
	else{
		echo "<br><br><div style='padding: 0 3%;'>Nessuna email è ancora stata aperta</div>";
	}
	$result=$mysqli->query("SELECT * FROM tracklink WHERE Cliente = 'alcafood' ORDER BY Data DESC");
	if ($result->num_rows > 0) {
		echo "<br><br><table class='table' style='padding: 0 3%;'><tr><th>Email</th><th>Data azione (click su link dem)</th><th>Soggetto</th></tr>";
		while($row = $result->fetch_assoc()) {
			echo "<tr id='mail".$row["Email"]."'><td>".$row["Email"]."</td><td>".$row["Data"]."</td><td>".$row['Soggetto']."</td></tr>";
		}
		echo "</table>";
	} else {
		echo "<br><br><div style='padding: 0 3%;'>Nessuna azione è stata ancora eseguita</div>";
	}
}

//funzione che tenta un singolo invio di tutte le mail non inviate presenti nella coda TODO decidere se fare solo per il singolo modello
function coda($mysqli,$conf, $modello){
	$result=$mysqli->query("SELECT idmail FROM ".$conf["tabella"]." WHERE inviato=0");
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			sendSingleMail($conf,$row["idmail"],$mysqli);
		}
		echo "Tentativo d'invio completato.";
	} else {
		echo "Non ci sono mail da inviare.";
	}
}

//funzione che cancella una singola mail dato il suo id preso da $_POST
function deleteMail($mysqli,$conf){
	$mysqli->query("DELETE FROM ".$conf["tabella"]." WHERE idmail=".$_POST["deleteId"]);
	exit;
}?>


<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
		#content{
			padding: 1% 3%;
		}
	</style>
	<script type="text/javascript">
		function disabilita(){
			document.getElementById("disabilitato").style.display="block";
			document.getElementById("invia").style.display="none";
		}
		function editable(option){
			if(option=="edit"){
				document.getElementById("textedit").style.display="block";
				document.getElementById("subject").style.display="block";
			}else{
				document.getElementById("textedit").style.display="none";
				document.getElementById("subject").style.display="none";
			}
		}
		function cambiaButton(id){
			document.getElementById("deleteButton"+id).className="btn btn-success";
			document.getElementById("deleteButton"+id).innerHTML="&#10003;";
			document.getElementById("deleteButton"+id).disabled="true";
			$("#mail"+id).fadeOut("slow");
		}
	</script>
</head>
<body onload="editable(document.getElementById('modello').value);">
	<div id="content">
	<h2><?=$conf["title"];?></h2>
	Destinatari (separare con a capo):<br>
	<textarea rows="10" cols="50" name="destinatari" form="form"></textarea><br>
	<div  id="textedit" style="display:none">
		<br>Testo personalizzato (accetta anche HTML):<br>
		<textarea rows="10" cols="50" name="textedit" form="form"></textarea>
	</div>
	<form action="index.php" method="post" id="form">
		<div id="subject" style="display:none">
			<br>Soggetto personalizzato:<br>
			<input type="text" name="subject">
		</div>
		<br><label for "modello">Soggetto: </label>
		<select name="modello" id="modello" onchange="editable(this.value)">
			<?php
				foreach($conf["modelli"] as $modello){
					echo "<option value='".$modello["file"]."'>".$modello["nome"]."</option>";
				}
			?>
		</select>
		<br><br>
		<div id="disabilitato" style="display:none;">Invio delle mail in corso, si prega di attendere...</div>
		<input type="submit" id="registro" name="action" value="Registro invii">
		<input type="submit" id="invia" name="action" value="Invia" onclick="disabilita()">
		<input type="submit" id="invia" name="action" value="Riprova" onclick="disabilita()">
		<input type="button" value="Anteprima" onclick="window.open('modelli/'+document.getElementById('modello').value+'.html','_blank')"><br><br><br><br>
		<input type="submit" id="registroapertura" name="action" value="Registro apertura">
		<input type="submit" id="registroazione" name="action" value="Registro azione">
		<input type="submit" id="registroprivacy" name="action" value="Registro privacy"><br><br>
		<!--<input type="submit" name="action" value="Oggetto alternativo">-->
	</form>
	<?php
	?>
	</div>
	<script
	  src="https://code.jquery.com/jquery-3.1.1.min.js"
	  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	  crossorigin="anonymous">
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.deleteButton').click(function(){
		        var clickBtnValue = $(this).val();
		        var ajaxurl = 'index.php',
		        data =  {'action': 'deleteMail', 'deleteId':clickBtnValue};
		        $.post(ajaxurl, data, function (response) {//TODO possibile restituire un messaggio con suggesso o no tramite response
		            cambiaButton(clickBtnValue);
		        });
		    });
		});
	</script>
</body>
</html>


<?php

   if(isset($_POST['action'])){
	switch ($_POST['action']){
		case "Invia":
			sendMailDB($conf,$mysqli);
			break;
		case "Anteprima":
			anteprima();
			break;
		case "Registro invii":
			registro($mysqli,$conf,$_POST["modello"]);
			break;
		case "Registro privacy":
			registroprivacy($conf,$mysqliPrivacy);
			break;
		case "Registro apertura":
			registroapertura($conf,$mysqliPrivacy);
			break;
		case "Registro azione":
			registroazione($conf,$mysqliPrivacy);
			break;
		case "Riprova":
			coda($mysqli,$conf, $_POST["modello"]);
			break;
		case "deleteMail":
			deleteMail($mysqli,$conf);
			break;
	}
  }
}

else{
	if(isset($_POST["password"]) && $_POST["password"]==$conf["password"]){
		$_SESSION["login"]=$conf["title"];
		header("Refresh:0");
	}
?>



<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
		#content{
			padding: 1% 3%;
		}
	</style>
</head>
<body>
	<div id="content">
	<h2><?=$conf["title"];?></h2>
	<form action="index.php" method="post" id="formlogin">

		<input type="password" name="password">

		<input type="submit" id="login" name="action" value="Accedi">
	</form>
	<?php
	//	if (isset($_POST['action']) && $_POST['action']=='Oggetto alternativo') subject();
	?>
	</div>
</body>
</html>

<?php
}
